import React from "react";
import { Row, Col, Button, Table, Form } from "react-bootstrap";
import OGAxios from "../../apis/OGAxios";
import { withNavigation, withParams } from "../../routeconf";

class Foods extends React.Component {
  constructor(props) {
    super(props);

    let search = {
      id: "",
      naziv: "",
      totalPages: "",
    };

    this.state = {
      foods: [],
      search: search,
      kategorije: [],
      pageNo: 0,
      totalPages: 2,
      check: true,
    };
  }

  componentDidMount() {
    this.getFoods(0);
    this.getKategorije();
  }

  getFoods(newPageNo) {
    if (newPageNo < 0) {
      newPageNo = 0;
    }

    console.log(newPageNo);

    let search = this.state.search;
    let config = {
      params: {
        id: search.id,
        naziv: search.naziv,
        pageNo: newPageNo,
      },
    };

    OGAxios.get("/jelovnik", config)

      .then((res) => {
        this.setState({ pageNo: newPageNo });
        console.log(res);
        this.setState({
          foods: res.data,
          totalPages: res.headers["total-pages"],
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  getKategorije() {
    OGAxios.get("/kategorije")
      .then((res) => {
        console.log(res);
        this.setState({ kategorije: res.data });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  onSearchInputChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    let search = this.state.search;
    search[name] = value;
    console.log(search);
    this.setState({ search: search });
    this.getFoods(0);
  }

  goToEditFood(id) {
    this.props.navigate("/jelovnik/edit/" +id);
  }

  deleteFood(id) {
    OGAxios.delete("/jelovnik/" + id)
      .then((res) => {
        console.log(res);
        alert("Uspesno obrisana stavka")
        this.getFoods(pageNo)

      })
      .catch((err) => {
        console.log(err);
        window.location.reload();
      });
  }

  renderKategorijeInDropDown() {
    return this.state.kategorije.map((kategorija) => {
      return (
        <option value={kategorija.id} key={kategorija.id}>
          {kategorija.naziv}
        </option>
      );
    });
  }


  renderFoods() {
    return this.state.foods.map((food) => {
      return (
        <tr key={food.id}>
          <td>{food.naziv}</td>
          <td>{food.kategorija.naziv}</td>
          <td>{food.cena}</td>
          {window.localStorage.getItem("role") === "ROLE_ADMIN" ?
            <>
              <td><Button variant="danger" onClick={() => this.deleteFood(food.id)}>Obrisi</Button></td>
              <td><Button onClick={() => this.goToEditFood(food.id)}> Edit</Button></td>
            </>
            : <td></td>}
          <td>
            {" "}
          </td>
        </tr>
      );
    });
  }


  render() {
    return (
      <>
        <Form.Check onChange={() => this.setState({ checked: !this.state.checked })} label="Sakrij pretragu" />

        {window.localStorage["role"] == "ROLE_ADMIN" ||
          window.localStorage["role"] == "ROLE_KORISNIK" ? (
          <div>
            <div className="search">
              <Form hidden={this.state.checked} style={{ width: "100%" }}>

                <Row>
                  <Col>
                    <Form.Label>Name</Form.Label>
                    <Form.Control
                      name="naziv"
                      as="input"
                      type="text"
                      onChange={(e) => this.onSearchInputChange(e)}
                    ></Form.Control>
                  </Col>
                </Row>


                <Row>
                  <Col>
                    <Form.Group>
                      <Form.Label>Category</Form.Label>
                      <Form.Select
                        name="id"
                        as="input"
                        type="number"
                        onChange={(e) => this.onSearchInputChange(e)}
                      >
                        <option value=""></option>
                        {this.renderKategorijeInDropDown()}
                      </Form.Select>
                    </Form.Group>
                  </Col>
                </Row>


              </Form>
              <Row>
                <Col>
                  {/* <Button onClick={() => this.getFoods(0)}>
                    Pretrazi
                  </Button> */}
                </Col>
              </Row>
            </div>
            <br />
            <div className="displayCompetitorTable">
              <Button
                style={{ margin: 3, width: 90 }}
                disabled={this.state.pageNo == 0} onClick={() => this.getFoods(this.state.pageNo - 1)}>
                Previous
              </Button>
              <Button
                style={{ margin: 3, width: 90 }}
                disabled={this.state.pageNo == this.state.totalPages - 1} onClick={() => this.getFoods(this.state.pageNo + 1)}>
                Next
              </Button>
              <Table>
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Price</th>

                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>{this.renderFoods()}</tbody>
              </Table>
              <br />
            </div>
          </div>
        ) : null}
      </>
    );
  }
}

export default withNavigation(withParams(Foods));
