import React from "react";
import { Row, Col, Button, Table, Form } from "react-bootstrap";
import OGAxios from "../../apis/OGAxios";
import { withParams, withNavigation } from "../../routeconf";

class EditFood extends React.Component {
  constructor(props) {
    super(props);

    let edit = {
      naziv: "",
      kategorijaId: "",
      cena: "",
    };

    this.state = {
      edit: edit,
      kategorije: [],
    };
  }

  componentDidMount() {
    this.getFood(this.props.params.id);
    this.getKategorije();
  }


  getFood(id) {
    OGAxios.get("/jelovnik/" + id)
      .then((res) => {
        console.log(res);
        this.setState({ edit: res.data });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  edit() {
    console.log(this.state.edit)
    var params = this.state.edit;
    OGAxios.put("/jelovnik/" + params.id, params)
      .then((res) => {
        console.log(res);
        this.props.navigate("/jelovnik");
      })
      .catch((error) => {
        console.log(error);
      });
  }



  getKategorije() {
    OGAxios.get("/kategorije")
      .then((res) => {
        console.log(res);
        this.setState({ kategorije: res.data });
      })
      .catch((err) => {
        console.log(err);
      });
  }


  renderKategorijeInDropDown() {
    return this.state.kategorije.map((kategorija) => {
      return (
        <option value={kategorija.id} key={kategorija.id}>
          {kategorija.naziv}
        </option>
      );
    });
  }


  onEditInputChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    let edit = this.state.edit;
    edit[name] = value;
    console.log(edit);
    this.setState({ edit: edit });
  }


  render() {
    return (
      <div>
        <div className="search">
          <Form style={{ width: "100%" }}>
            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>Name</Form.Label>
                  <Form.Control
                    name="naziv"
                    as="input"
                    type="text"
                    value={this.state.edit.naziv}
                    onChange={(e) => this.onEditInputChange(e)}
                  ></Form.Control>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>Price</Form.Label>
                  <Form.Control
                    name="cena"
                    as="input"
                    type="number"
                    min = "0"
                    step ="50"
                    value={this.state.edit.cena}
                    onChange={(e) => this.onEditInputChange(e)}
                  ></Form.Control>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>Category</Form.Label>
                      <Form.Select
                        name="kategorijaId"
                        as="input"
                        type="number"
                        onChange={(e) => this.onEditInputChange(e)}
                      >
                        <option value=""></option>
                        {this.renderKategorijeInDropDown()}
                      </Form.Select>
                </Form.Group>
              </Col>
            </Row>
          </Form>
          <Row>
            <Col>
              <Button onClick={() => this.edit()}>Izmeni</Button>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}
export default withParams(withNavigation(EditFood));
