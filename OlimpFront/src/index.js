import React from "react";
import ReactDOM from "react-dom";
import {
  Route,
  Link,
  HashRouter as Router,
  Routes,
} from "react-router-dom";
import { Navbar, Nav, Button, Container } from "react-bootstrap";
import Foods from "./components/Olimp/Foods";
import { logout } from "./services/auth";
import Login from "./components/Login/Login";
import EditFood from "./components/Olimp/EditFood";

class App extends React.Component {
  render() {
    return (
      <>
        <Router>
          <Navbar expand bg="dark" variant="dark">
            <Navbar.Brand as={Link} to="/jelovnik">
              JWD
            </Navbar.Brand>
            {window.localStorage["role"] == "ROLE_ADMIN" ||
            window.localStorage["role"] == "ROLE_KORISNIK" ? (
              <Nav>
                <Nav.Link as={Link} to="/jelovnik">
                  Menu
                </Nav.Link>
              </Nav>
            ) : null}

            {window.localStorage["jwt"] ? (
              <Button onClick={() => logout()}>Log out</Button>
            ) : (
              <Nav.Link as={Link} to="/login">
                Log in
              </Nav.Link>
            )}
          </Navbar>
          <Container style={{ paddingTop: "10px" }}>
            <Routes>
              <Route path="/login" element={<Login />} />
              <Route path="/jelovnik" element={<Foods />} />
              <Route path="/jelovnik/edit/:id" element={<EditFood />} />
            </Routes>
          </Container>
        </Router>
      </>
    );
  }
}

ReactDOM.render(<App />, document.querySelector("#root"));
