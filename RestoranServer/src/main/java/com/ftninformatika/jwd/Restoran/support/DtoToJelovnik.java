package com.ftninformatika.jwd.Restoran.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.Restoran.dto.JelovnikDto;
import com.ftninformatika.jwd.Restoran.model.Jelovnik;
import com.ftninformatika.jwd.Restoran.service.JelovnikService;
import com.ftninformatika.jwd.Restoran.service.KategorijaService;
//import com.ftninformatika.jwd.Restoran.service.KategorijaService;
@Component
public class DtoToJelovnik implements Converter<JelovnikDto, Jelovnik> {

	@Autowired
	private JelovnikService service;
	
	@Autowired
	private KategorijaService ktService;
	
	
	@Override
	public Jelovnik convert(JelovnikDto dto) {
		Jelovnik entity;
		if(dto.getId()==null) {
			entity=new Jelovnik();
		}else {
			entity=service.findOneById(dto.getId());
		}
		
		
		if(entity!=null) {
			entity.setNaziv(dto.getNaziv());
			entity.setCena(dto.getCena());
			
			
			if(dto.getKategorijaId()!=null && ktService.findOneById(dto.getKategorijaId())!=null) {
				entity.setKategorija(ktService.findOneById(dto.getKategorijaId()));
			}
			
		}
		
		return entity;
	}
}
