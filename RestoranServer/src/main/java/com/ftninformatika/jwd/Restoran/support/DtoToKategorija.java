package com.ftninformatika.jwd.Restoran.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.Restoran.dto.KategorijaDto;
import com.ftninformatika.jwd.Restoran.model.Kategorija;
import com.ftninformatika.jwd.Restoran.service.KategorijaService;
@Component
public class DtoToKategorija implements Converter<KategorijaDto, Kategorija> {

	@Autowired
	private KategorijaService service;
	
	@Override
	public Kategorija convert(KategorijaDto dto) {
		Kategorija entity;
		if(dto.getId()==null) {
			entity=new Kategorija();
		}else {
			entity=service.findOneById(dto.getId());
		}
		
		if(entity!=null) {
			entity.setNaziv(dto.getNaziv());
		}
		
		return entity;
	}

}
